## Digital Cam List
Semplice app scritta con il framework ionic v2 che contiene una lista con alcune videocamere
digitali insieme ad una breve descrizione, tipico utilizzo e altri dettagli tecnici.

Per tutte le informazioni su ionic c'è la [documentazione](https://ionicframework.com/docs)

I dati sulle videocamere sono scritti a mano per ora e si trovano in
*src/pages/cam-list/cam-list.ts* nell'array *cache*.

### Informazioni

###### In generale
L'applicazione ha una struttura semplice, dopo lo splash screen su android o direttamente sul browser si apre una home page che serve ad introdurre l'interfaccia. La navigazione nel menu laterale è data da uno dei template di ionic (sidemenu) modificato per le necessità del progetto.

Tramite il menu si può accedere ad un'altra pagina fondamentale cioè la lista di videocamere, da cui selezionandone una si arriva alla pagina di dettagli di quella specifica camera (la pagina è uno scheletro unico per tutte e viene riempita con i dati adatti ogni volta).

La card di benvenuto nella pagina iniziale si può modificare facilmente dal file src/pages/home/home.html dato che non ha bisogno di codice. Per le altre pagine serve modificare anche i file .ts correlati.

###### Dietro le quinte
La lista di camere sfrutta attributi nei tag detti direttive definiti dal framework [angular](https://angularjs.org/) inserite nei tag html che ci fornisce ionic (ion-content, ion-card, ecc) come ad esempio **ngFor* che serve a generare elementi html dinamicamente in base ad una variabile passata tramite codice.

In pratica:
quando si naviga nell'app fino alla pagina cam-list viene eseguito il codice che si trova nella classe *CamListPage* (in src/pages/cam-list/cam-list.ts). Qui prima di mostrare la pagina html viene riempito il template, ovvero il costruttore di CamListPage (funzione eseguita appena si crea un oggetto) fa il suo dovere di riempire la variabile *items* con la nostra lista di videocamere che si trova in *cache*. Subito prima di mostrare l'html si eseguono le direttive angular per elaborare il file, e quindi per **ngFor*, se esiste un oggetto *items* viene creata una ion-card per ognuno dei suoi elementi, con una funzione associata all'evento di click sulla card ovvero itemTapped.

Quando si clicca su una delle videocamere e viene eseguita itemTapped, l'app aprirà la pagina cam-details passandole come parametro l'elemento della lista che abbiamo selezionato. CamDetailsPage è la classe che quindi si occupa di prendere questo parametro e inserirlo in una sua variabile disponibile quindi al template per riempire il file cam-details.html e mostrare finalmente la pagina all'utente. Qui la direttiva **ngIf* serve a controllare che esista la variabile *selectedItem* che è stata impostata da *CamDetailsPage* ed arriva dalla funzione attaccata all'evento di click in *CamListPage*.

###### Ulteriori modifiche
Per modificare l'interfaccia di una pagina editare il file html nella cartella specifica.
Ad esempio per modificare la home il file è src/pages/home/home/.html idem per le altre pagine.

Lo stesso discorso vale per modificare gli stili, si editano i file Sass (.scss) rispettivamente in
src/app/app.scss per le variabili globali e src/pages/page/page.scss per lo stile locale di una pagina.

Per la logica dell'applicazione o delle pagine specifiche si modificano i file .ts in src/app o nella cartella di una pagina in particolare.

### Sviluppo
I comandi successivi si intendono da scrivere in un terminale aperto nella cartella principale del progetto.

###### Preparazione
Installare

- il framework [ionic](https://ionicframework.com)

- il gestore di pacchetti npm, ottenibile con [nodejs](https://nodejs.org/en/)

- il programma [cordova](https://cordova.apache.org) globalmente con
``` npm install -g cordova ```

- le dipendenze del progetto
``` npm install ```

###### Web
Per lanciare l'app via browser non serve altro, aprire un terminale e scrivere

``` ionic serve ```

###### Android
Su Android ci sono alcuni passi aggiuntivi da fare:

- installare il kit di sviluppo [Android](https://developer.android.com/studio/index.html)

- tramite sdk manager installare il pacchetto *platform-tools* e i *driver usb* di google

- sul dispositivo attivare il *debug usb* dalle opzioni sviluppatore

Dopodichè da terminale scrivere

``` cordova platform add android ```

``` ionic run --debug android ```

### Struttura
   Percorso       |   Descrizione
---------------   | -------------
**hooks/**        | si possono aggiungere scripts che girano durante la compilazione tramite cordova
**node_modules/** | cartella usata da npm per installare le dipendenze del progetto
**platforms/**    | codice specifico per iOs e Android
**plugins/**      | plugins cordova usati dall'applicazione
**resources/**    | cartella dove risiedono l'icona e lo splash screen per piattaforme mobile
**www/**          | tutto il codice compilato e altre risorse si trovano qui
**src/**          | il codice sorgente vero e proprio dell'app è questo, scritto in TypeScript che verrà poi compilato in JavaScript
src/**theme/**    | tema e stili si possono modificare da qui, o dal file Sass globale in src/app/ 
src/**app/**      | nucleo effettivo dell'applicazione
src/app/**app.module.ts**     | punto di partenza per il modulo che stiamo scrivendo (l'app)
src/app/**app.component.ts**  | primo componente che di solito è solo un guscio vuoto usato per caricarne altri
src/app/**app.html**          | il template principale per l'interfaccia
src/app/**app.scss**          | foglio di stile globale (verrà compilato in css)
src/**pages/**                | tutte le varie pagine vengono tenute qui per organizzarne meglio la struttura
src/**home/**                 | esempio di struttura di una pagina
src/pages/home/**home.html**  | template per la pagina, qui solo una card con titolo e un breve paragrafo
src/pages/home/**home.scss**  | stile della pagina, si applicano prima le regole globali e poi queste
src/pages/home/**home.ts**    | la logica della pagina, vuota perchè non c'è niente da fare nella home
**index.html**        | la pagina html base dove mettere i vari meta tag e scripts/links (nell'head)
**manifest.json**     | informazioni utili ad altri programmi come nome dell'app, icona, ecc
**service-worker.js** | non utilizzato, serve a scrivere operazioni in background
**config.xml**        | impostazioni usate da cordova per compilare su altre piattaforme
**ionic.config.json** | impostazioni specifiche del progetto (fondamentalmente nome e id)
**package.json**      | file usato da npm per conoscere le dipendenze da scaricare
**tsconig.json**      | configurazioni per il compilatore TypeScript
**tslint.json**       | impostazioni per il linter (analizza gli errori sintattici nel codice)
