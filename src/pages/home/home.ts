import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

// non c'è una particolare logica nella home quindi la classe è vuota
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  constructor(public navCtrl: NavController) {}
}
