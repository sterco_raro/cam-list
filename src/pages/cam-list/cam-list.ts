import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CamDetailsPage } from '../cam-details/cam-details';

// lista di dati sulle videocamere, per ora è tutto scritto qui
// ogni videocamera ha:
//   @title - il nome completo (marca e modello)
//   @src   - il nome dell'immagine
//   @desc  - la descrizione che appare sulla pagina di dettagli
let cache = [
  { title: 'Arri Alexa 65', src: 'arri-alexa-65.png', desc: 'Una vera e propria macchina da presa esclusivamente per il cinema, anzi solo per un certo cinema di livello. Sensore 65mm 6K @ 60fps, uncompressed ARRIRAW' },
  { title: 'Blackmagic Cinema Camera 4K', src: 'blackmagic-cinema-camera-4k.png', desc: 'Camera molto usata per cortometraggi e videoclip. Sensore super 35mm 4K @25fps cinema RAW DNG' },
  { title: 'Blackmagic Ursa Mini Pro', src: 'blackmagic-ursa-mini-pro.png', desc: 'Ottima camera per documentari, matrimoni o spot pubblicitari di alto livello. Sensore super 35mm 4k' },
  { title: 'Canon 5D MK4', src: 'canon-5d-mk4.png', desc: 'Ottima camera per documentari, matrimoni o spot pubblicitari di medio livello. Sensore full frame 4K, HD fino a 120fps ISO fino a 102.400' },
  { title: 'Canon C 300', src: 'canon-c-300.png', desc: 'Molto usata per la televisione ed i documentari. Sensore super 35mm 4K @ 60fps' },
  { title: 'Canon XC15', src: 'canon-xc15.png', desc: 'Piccola e leggera,  ottima per matrimoni o videoclip amatoriali obiettivo fisso con un sensore un pollice in 4K' },
  { title: 'DJI Mavic Pro', src: 'dji-mavic-pro.png', desc: 'Drone molto leggero e richiudibile usato molto nei matrimoni e soprattutto nei viaggi. Risoluzione 4K, FULLHD @ 96fps con gimbal elettronico a 3 assi per la stabilizzazione dell\' immagine' },
  { title: 'DJI Phantom 4', src: 'dji-phantom-4-pro.png', desc: 'Uno dei droni top di gamma con dimensioni medio/piccole, sensore un pollice 4K @ 96fps con gimbal elettronico a 3 assi per la stabilizzazione dell\' immagine' },
  { title: 'Panasonic GH5', src: 'panasonic-gh5.png', desc: 'Una piccola grande macchina molto usata per spot, documentari e matrimoni. Sensore stabilizzato a 5 assi micro 4/3 6K @60fps' },
  { title: 'Sony A6500', src: 'sony-a6500.png', desc: 'Molto usata per spot, matrimoni e sott\'acqua per le sue dimensioni ridotte. Sensore APSC 4K con uno slowmotion fino a 120fps' },
  { title: 'Sony A9', src: 'sony-a9.png', desc: 'Ottima camera per documentari, matrimoni o spot pubblicitari di medio livello. Sensore full frame 4k, HD fino a 120fps' },
  { title: 'Sony Venice', src: 'sony-venice.png', desc: 'Una camera per il cinema e le grandi produzioni, di grandi dimensioni. Sensore anamorfico full frame 35mm 6K RAW con 8 step ND meccanico' },
  { title: 'Red Weapon', src: 'red-weapon-8k.png', desc: 'Camera esclusiva per il cinema e grandi spot pubblicitari. Sensore super 35mm risoluzione 8K @75fps con 16,5 stop di range dinamico' },
];

@Component({
  selector: 'page-cam-list',
  templateUrl: 'cam-list.html'
})
export class CamListPage {

  // la lista di oggetti da mostrare
  items: Array<{title: string, imgsrc: string, shodesc: string}>;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.items = [];

    // usa la cache per riempire @items con i dati sulle videocamere
    for (let i = 0; i < cache.length; i++) {
      // per ogni videocamera, crea un oggetto
      this.items.push({
        title: cache[i].title,
        imgsrc: 'images/' + cache[i].src, // aggiungiamo un prefisso che si riferisce a www/images
        shodesc: cache[i].desc
      });
    }
  }

  itemTapped(event, item) {
    // eseguita sul click di un elemento della lista
    // reindirizziamo la navigazione alla pagina dei dettagli
    // passando @item per riempire il template
    this.navCtrl.push(CamDetailsPage, {
      item: item
    });
  }
}
