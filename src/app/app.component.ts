import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { CamListPage } from '../pages/cam-list/cam-list';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  // imposta la pagina iniziale
  rootPage: any = HomePage;

  // lista di pagine disponibili
  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // ogni pagina ha un titolo e un component, ovvero il codice che ne gestisce la logica
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Cameras', component: CamListPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // funzioni necessarie su mobile
      // nasconde lo splash screen e imposta lo stile di defaultdella status bar
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // chiamata dal menu laterale per aprire un'altra pagina
    // setRoot imposta la pagina attuale a quella passata come parametro
    this.nav.setRoot(page.component);
  }
}
